add_executable(tcomponents tcomponents.cc)

target_include_directories(tcomponents PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/askap>
    $<INSTALL_INTERFACE:include>
    $<INSTALL_INTERFACE:include/askap>
    ${log4cxx_INCLUDE_DIRS}
    ${CASACORE_INCLUDE_DIRS}
    ${COMPONENTS_INCLUDE_DIRS}
)


target_link_libraries(tcomponents
	components
	${CPPUNIT_LIBRARY}
)
add_test(
	NAME tcomponents
	COMMAND tcomponents
	)
