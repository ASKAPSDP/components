// std include
#include <string>

#ifndef ASKAP_ASKAP_H
#define ASKAP_ASKAP_H

  /// The name of the package
#define ASKAP_PACKAGE_NAME "components"

/// askap namespace
namespace askap {
  // @return version of the package
  std::string getAskapPackageVersion_components();
}

/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_components()

#endif
